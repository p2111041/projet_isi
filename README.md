# Projet_ISI

Projet de création de site internet pour l'UE ISI 
Réalisé par GONZALEZ Anthony & MORIN Alice
Notre projet consiste en la construction d’un site internet d’informations sur des groupes féminin de pop coréenne, dite KPOP.

## Description

Notre site affiche et permet de modifier des données d'une base de données à l'aide du Framework php Laravel. 
Ce site se découpe en 3 pages principales qui permettent d'afficher les différents groupes, les différents chanteurs des groupes ainsi que leurs musiques.
Nous avons une fonctionnalité de connexion qui est nécessaire pour visualiser le contenu vidéo ainsi que sa modification.
L'utilisateur connecté peut aussi ajouter des musiques à sa guise.

## Visuals

![](presentation.gif)

## Installation

Pour installer ce programme, il faut tout d'abord créer une base de donnée et lui associer un utilisateur userkpop ayant pour mot de passe C1Secret!.

+ `create database KPOP;`

+ `create user userkpop@localhost identified by 'C1Secret!';`

+ `grant all privileges on KPOP *. ** to userkpop@localhost with grant option;`

+ `flush privileges;`

*La liste des commandes est aussi disponible dans le fichier create_user.sql .*

Il faut ensuite cloner le projet à l'aide de la commande `git clone https://forge.univ-lyon1.fr/p2111041/projet_isi.git`

Dans le fichier .env (ne pas oublier de copier l'exemple puis de le renommer) on rempli les champs de la connexion mysql avec l'authentification précédente.

![](./projetISI/imagesreadme/screen_env.png)
 
On met à jour la clé du site en faisant : `php artisan key:generate`.

On met ensuite à jour composer pour le projet existant en faisant : `composer update`

Une fois cela réalisé, il faut maintenant lancer les migrations. On utilise la commande php artisan migrate.

Pour finir, il faut ajouter les données à l'aide du script insert.sql .


## Authors and acknowledgment
Anthony GONZALEZ & Alice MORIN
