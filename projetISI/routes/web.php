<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupeController;
use App\Http\Controllers\ChanteurController;
use App\Http\Controllers\MusiqueController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[GroupeController::class,'index']);

Route::get('/dashboard', function () {
    return redirect('/groupes');
});

Route::resource('groupes', GroupeController::class);
Route::resource('chanteurs', ChanteurController::class);
Route::resource('musiques', MusiqueController::class);

require __DIR__.'/auth.php';