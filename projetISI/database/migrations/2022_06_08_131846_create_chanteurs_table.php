<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChanteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chanteurs', function (Blueprint $table) {
            $table->id();
            $table->text('nom_de_scene');
            $table->text('nom_reel');
            $table->text('nationalite');
            $table->date('date_naissance');
            $table->unsignedBigInteger('groupe_id');
            $table->foreign('groupe_id')
                ->references('id')
                ->on('groupes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->text('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chanteurs');
    }
}
