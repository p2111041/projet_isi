@extends('layouts.layout')

@section('titrePage')
    Chanteurs
@endsection

@section('titreItem')
    <h1>{{$groupe->nom_groupe}} </h1>
@endsection

@section('contenu')

    <body>
        <div class="chanteurs">
            @foreach($chanteurs as $chanteur)
                            <div class="cadre"> 
                                <span>{{$chanteur->nom_reel}} </span><br>
                                <span>{{$chanteur->nationalite}} </span><br>
                                <span>{{$chanteur->date_naissance}}</span><br>
                                <?php echo '<img src="../images/'.$chanteur->photo.'" class="card-img-top">';?>
                            </div>
                    <h3 class="name">{{ $chanteur->nom_de_scene }}</h3>
            @endforeach
        </div>

        <div class="mti">
            @foreach($musiques as $musique)
            <a href="{{ route('musiques.show', $musique->id) }}">
                <div class="musiques">
                    <div class="titre">
                        <p> {{ $musique->titre}} </p>
                    </div>
                    <div class="annee">
                        <p> {{ $musique->annee_de_sortie}} </p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

        <br>

        @auth
        <div class="cbtt">
            <a href="{{route('musiques.create')}}" class="btt">
                Ajouter une musique 
            </a>
        </div>
        @else
        @endauth

        <br>
        
    </body>
@endsection