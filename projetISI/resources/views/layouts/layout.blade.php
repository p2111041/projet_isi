<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href=" {{ URL::asset('css/kpop.css') }}">

        <title>
            @yield('titrePage')
        </title>

    </head>
    <body>
        <header>
        <nav class="navbar navbar-dark bg-dark">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls=#navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">@yield('titreItem')</a>
            </div>
            <div class="collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('groupes.index')}}">Home</a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" id="deconnexion" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Déconnexion
                        </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link active" id="connexion" href="{{ route('login')}}">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="enregistrer" href="{{ route('register')}}">S'enregistrer</a>
                    </li>
                    @endauth
                </ul>
            </div> 
        </nav> 
        </header>

        @yield('contenu')

        <footer class="footer">
            GONZALEZ Anthony & MORIN Alice - copyright 3A Info - 2022
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>