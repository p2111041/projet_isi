@extends('layouts.layout')

@section('titrePage')
    Modification d'une musique
@endsection

@section('contenu')
<body>
    <div class = "container">
        <div class ="row card text-white bg-dark">
            <h4 class ="card-header"> Modifier une musique </h4>
            <div class ="card-body">
                <form action="{{route('musiques.update', $musique->id)}}" method="POST">
                    @csrf
                    @method('put')
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control @error('lien_musique') is-invalid @enderror" name="lien_musique" id="lien_musique" placeholder="Lien de la musique">
                        @error('lien_musique')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-secondary"> Envoyer</button>
                </form>
                
            </div>
        </div>
    </div>
</body>
@endsection
