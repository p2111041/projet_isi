@extends('layouts.layout')

@section('titrePage')
    Ajout d'une musique
@endsection

@section('contenu')
<body>
    <div class = "container">
        <div class ="row card text-white bg-dark">
            <h4 class ="card-header"> Ajouter une musique </h4>
            <div class ="card-body">
                <form action="{{route('musiques.store')}}" method="POST">
                    @csrf
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control @error('titre') is-invalid @enderror" name="titre" id="titre" placeholder="Titre de la musique">
                        @error('titre')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control @error('lien_musique') is-invalid @enderror" name="lien_musique" id="lien_musique" placeholder="Lien de la musique">
                        @error('lien_musique')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control @error('album') is-invalid @enderror" name="album" id="album" placeholder="Album de la musique">
                        @error('album')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control @error('annee_de_sortie') is-invalid @enderror" name="annee_de_sortie" id="annee_de_sortie" placeholder="Année de sortie de la musique">
                        @error('annee_de_sortie')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <br>
                    <div class="form-group">
                        <select type="text" class="form-control @error('groupe_id') is-invalid @enderror" name="groupe_id" id="groupe_id">
                            <option value="1">BlackPink</option>
                            <option value="2">Twice</option>
                            <option value="3">Red Velvet</option>
                            <option value="4">Aespa</option>
                            <option value="5">IVE</option>
                            <option value="6">Weki Meki</option>
                            <option value="7">(G)I-DLE</option>
                        </select>
                        @error('groupe_id') 
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-secondary"> Envoyer</button>
                </form>
                
            </div>
        </div>
    </div>
</body>
@endsection
