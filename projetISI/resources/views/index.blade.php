@extends('layouts.layout')

@section('titrePage')
    Accueil
@endsection

@section('titreItem')
    <h1>Groupes </h1>
@endsection

@section('contenu')

    <body>
        <div class="groupe">
            @foreach($groupes as $groupe)
            <a href="{{ route('groupes.show', $groupe->id) }}"><h1> {{ $groupe->nom_groupe }} </h1></a>
                <a href="{{ route('groupes.show', $groupe->id) }}">
                    <div class=image>
                        <?php echo '<img class="mg" src="images/'.$groupe->photo_groupe.'">';?>
                    </div>
                </a><br>
            @endforeach
        </div>
    </body>
       
@endsection
