@extends('layouts.layout')

@section('titrePage')
    Musique
@endsection

@section('titreItem')
    <h1>{{$musique->titre}}</h1>
@endsection

@section('contenu')
    <body>
        @auth
        <div class="video-responsive"><iframe width="1200" height="600" src={{ $musique ->lien_musique}} allowfullscreen></iframe></div>
        <h3>Album : {{ $musique ->album}}, Année de sortie : {{ $musique ->annee_de_sortie}}</h3>
        <br>
        <div class="cbtt">
            <a href="{{route('musiques.edit', $musique->id)}}" class="btt">
                Modifier le lien
            </a>
        </div>
        <div class="cbtt">
            <a href="{{route('musiques.create')}}" class="btt">
                Ajouter une musique 
            </a>
        </div>
        <br>
        @else
        <a href="{{ route('login')}}"><h3>Il faut se connecter pour accéder à ce contenu</h3></a>
        @endauth
    </body>
@endsection
