<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertMusiqueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titre' => ['required', 'string', 'max:100'],
            'lien_musique'  => ['required', 'string', 'max:500'],
            'album' => ['required', 'string', 'max:100'],
            'annee_de_sortie'=> ['required', 'year', 'max:100'],
            'groupe_id' =>['required','int','max:2']
        ];
    }
}

