<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMusiqueRequest;
use App\Http\Requests\UpdateMusiqueRequest;
use App\Models\Musique;
use App\Models\Groupe;
use App\Http\Controllers\GroupeController;
use App\Http\Requests\InsertMusiqueRequest;

class MusiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMusiqueRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMusiqueRequest $request)
    {
        Musique::create($request->all());
        return view('confirm');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Musique  $musique
     * @return \Illuminate\Http\Response
     */
    public function show(Musique $musique)
    {
        return view('musique', compact('musique'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Musique  $musique
     * @return \Illuminate\Http\Response
     */
    public function edit(Musique $musique)
    {
        return view('edit', compact('musique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMusiqueRequest  $request
     * @param  \App\Models\Musique  $musique
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMusiqueRequest $request, Musique $musique)
    {
        $musique->update($request->all());
        return back()->with('info','La musique a bien été modifié dans la base de données.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Musique  $musique
     * @return \Illuminate\Http\Response
     */
    public function destroy(Musique $musique)
    {
        //
    }
}
