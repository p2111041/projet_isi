<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChanteurRequest;
use App\Http\Requests\UpdateChanteurRequest;
use App\Models\Chanteur;

class ChanteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChanteurRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChanteurRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chanteur  $chanteur
     * @return \Illuminate\Http\Response
     */
    public function show(Chanteur $chanteur)
    {
        $path = 'images/' .$chanteur->photo;
        return view('chanteur', compact('chanteur','path'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chanteur  $chanteur
     * @return \Illuminate\Http\Response
     */
    public function edit(Chanteur $chanteur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChanteurRequest  $request
     * @param  \App\Models\Chanteur  $chanteur
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChanteurRequest $request, Chanteur $chanteur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chanteur  $chanteur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chanteur $chanteur)
    {
        //
    }
}
