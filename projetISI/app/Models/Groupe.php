<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Groupe extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function chanteur()
    {
        return $this->hasMany(Chanteur::class);
    }

    public function musique()
    {
        return $this->hasMany(Musique::class);
    }
    
}
