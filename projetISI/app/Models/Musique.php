<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Musique extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable =['lien_musique'];
    
    public function groupes()
    {
        return $this->belongsTo(Groupe::class);
    }
}
